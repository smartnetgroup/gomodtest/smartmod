package test

import (
	"SmartMod"
	"testing"
)

func TestSmartMod(t *testing.T) {
	wantErr := 0
	if got := SmartMod.NewMod("Привет"); got != wantErr {
		t.Errorf("Код ошибки %d не равен ожиданию - %d", got, wantErr)
	}
}
